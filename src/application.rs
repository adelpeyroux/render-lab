use sp_gui::glutin as glutin;

use glutin::event::{Event, WindowEvent};
use glutin::event_loop::{ControlFlow, EventLoop};
use glutin::window::WindowBuilder;
use glutin::{ContextBuilder, WindowedContext};

use sp_gui::imgui::*;

use std::time::Instant;
use sp_renderer::{gl, gl::shader::shader_program::ShaderProgram};
use sp_renderer::gl::gl_enum::*;
use sp_renderer::graphical_objects::camera::Camera;
use sp_renderer::sp_scene::sp_mesh::sp_math::vector::{Vector3f, Vector2f};
use sp_renderer::sp_scene::Scene;
use sp_renderer::graphical_objects::scene_renderer::SceneRenderer;
use sp_renderer::render_graph::connexion::ConnexionData::{Boolean, Float, Scene as SceneData, Shader as ShaderData, Camera as CameraData};

pub struct Application {
    window_ctx: WindowedContext<glutin::PossiblyCurrent>,
    event_loop: EventLoop<()>,
    gui: sp_gui::ui::Ui,
    gui_renderer: sp_gui::renderer::Renderer,
    gl: gl::Gl,
}
use std::env;
use std::ffi::OsString;

impl Application {
    pub fn new () -> Self {
        let event_loop = EventLoop::new();
        let window_builder = WindowBuilder::new()
            .with_title("RenderLab")
            .with_inner_size(glutin::dpi::LogicalSize::new(800, 600));
        let window_ctx = ContextBuilder::new()
            .build_windowed(window_builder, &event_loop)
            .unwrap();

        let window_ctx = unsafe { window_ctx.make_current().unwrap() };

        let gl = sp_renderer::init(window_ctx.context());

        let (gui, gui_renderer) = sp_gui::init(window_ctx.context());

        Application {
            window_ctx,
            event_loop,
            gui,
            gui_renderer,
            gl,
        }
    }

    pub fn run (self) {
        let w_id = self.window_ctx.window().id();

        let start : Instant = Instant::now();

        let w_ctx = self.window_ctx;
        let el = self.event_loop;
        let gl = self.gl;

        let mut drawing = true;

        let mut win_size = Vector2f::new(800.0, 600.0);
        let mut gui = self.gui;
        let gui_renderer = self.gui_renderer;

        let mut render_graph = sp_renderer::render_graph::RenderGraph::new(&gl);

        let wireframe_node = render_graph.add_node(Box::new(
           sp_renderer::nodes::value_node::ValueNode::new(Boolean(false))
        ));

        let soft_node = render_graph.add_node(Box::new(
            sp_renderer::nodes::value_node::ValueNode::new(Boolean(true))
        ));

        let tess_node = render_graph.add_node(Box::new(
            sp_renderer::nodes::value_node::ValueNode::new(Float(1.0))
        ));

        let args: Vec<String> = env::args().collect();

        let scene = if args.len() > 1 {
            sp_renderer::sp_scene::io::obj::from_file(OsString::from(args[1].clone())).unwrap()
        } else {
            Scene::new()
        };

        let scene_rndr = SceneRenderer::new(&gl, &scene);
        let scene_node = render_graph.add_node(Box::new(
            sp_renderer::nodes::value_node::ValueNode::new(SceneData(scene_rndr))
        ));

        let mut shader = ShaderProgram::from_files(
            &gl,
            "./shaders/simple.vert".to_string(),
            "./shaders/simple.frag".to_string()
        );
        shader.add_tesselation_from_files(
            &gl,
            "./shaders/simple.tese".to_string(),
            "./shaders/simple.tesc".to_string()
        );
        shader.add_geometry_from_files(
            &gl,
            "./shaders/simple.geom".to_string()
        );

        shader.attach_shaders(&gl);
        shader.link(&gl);

        let shader_node = render_graph.add_node(Box::new(
            sp_renderer::nodes::value_node::ValueNode::new(ShaderData(shader))
        ));

        let camera_node = render_graph.add_node(Box::new(
            sp_renderer::nodes::value_node::ValueNode::new(CameraData(
                Camera::new(
                    Vector3f::new(2.0, 1.0, 2.0),
                    Vector3f::new(0.0, 0.0, 0.0)
                )
            ))
        ));

        let forward_node = render_graph.add_node(Box::new(
            sp_renderer::nodes::forward_node::ForwardNode::new(&gl)
        ));

        render_graph.connect_nodes(
            scene_node, "out_value".to_string(),
            forward_node, "in_scene".to_string(),
        );
        render_graph.connect_nodes(
            shader_node, "out_value".to_string(),
            forward_node, "in_shader".to_string(),
        );
        render_graph.connect_nodes(
            camera_node, "out_value".to_string(),
            forward_node, "in_camera".to_string(),
        );
        render_graph.connect_nodes(
            wireframe_node, "out_value".to_string(),
            forward_node, "in_wireframe".to_string(),
        );
        render_graph.connect_nodes(
            soft_node, "out_value".to_string(),
            forward_node, "in_soft".to_string(),
        );
        render_graph.connect_nodes(
            tess_node, "out_value".to_string(),
            forward_node, "in_tess_level".to_string(),
        );

        render_graph.connect_nodes(
            forward_node, "out_target".to_string(),
            0, "input_buffer".to_string(),
        );

        gui.set_display_size(win_size.x(), win_size.y());
        gui.set_alpha(0.82);

        let mut unifornm_opened = true;
        let mut tesselation_level: f32 = 1.0;
        let mut zoom_level: f32 = 1.0;
        let mut soft_shading = false;
        let mut wireframe = true;

        el.run(move |event, _, control_flow| {

            *control_flow = ControlFlow::Poll;//ControlFlow::WaitUntil(now + FRAME_DURATION);
            gui.set_display_size(win_size.x(), win_size.y());
            gui.dispatch_event(&event);
            match event  {
                Event::MainEventsCleared => {
                    w_ctx.window().request_redraw();
                },
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    window_id,
                } if window_id == w_id => {
                    gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

                    drawing = false;
                    *control_flow = ControlFlow::Exit;
                },
                Event::WindowEvent {
                    window_id: _,
                    event: WindowEvent::Resized(size)
                } => {
                    win_size = Vector2f::new(size.width as f32, size.height as f32);
                    gl.viewport(0,0, win_size.x() as i32, win_size.y() as i32);
                },
                Event::WindowEvent {
                    window_id: _,
                    event: WindowEvent::DroppedFile(path)
                } => {
                    let scene = sp_renderer::sp_scene::io::obj::from_file(
                        path.into_os_string()).unwrap();

                    render_graph.set_node_input(scene_node, "in_value".to_string(), SceneData(SceneRenderer::new(&gl, &scene)));
                },
                Event::RedrawRequested(_) if drawing => {
                    gui.update(w_ctx.window());

                    let ui = gui.frame();

                    let w = Window::new(im_str!("Uniforms"))
                        .opened(&mut unifornm_opened)
                        .position([20.0, 20.0], Condition::Appearing)
                        .size([700.0, 185.0], Condition::Appearing)
                        .resizable(false);
                    w.build(&ui, || {
                        let mut interact = false;
                        interact |= ui.drag_float(im_str!("Tesselation level"), &mut tesselation_level).min(1.0).max(10.0).speed(0.1).build();
                        interact |= ui.drag_float(im_str!("Zoom level"), &mut zoom_level).min(0.2).max(100.0).speed(0.1).build();
                        interact |= ui.checkbox(im_str!("Soft shading"), &mut soft_shading);
                        interact |= ui.checkbox(im_str!("Wireframe"), &mut wireframe);
                        if interact {
                            w_ctx.window().request_redraw();
                        }
                    });

                    let time = start.elapsed().as_secs_f32();

                    let x_cam = 2.0 * zoom_level * time.cos();
                    let z_cam = 2.0 * zoom_level * time.sin();
                    let camera = Camera::new(
                        Vector3f::new(x_cam, 1.0 * zoom_level, z_cam),
                        Vector3f::new(0.0, 0.0, 0.0)
                    );

                    render_graph.set_node_input(camera_node, "in_value".to_string(), CameraData(camera));
                    render_graph.set_node_input(wireframe_node, "in_value".to_string(), Boolean(wireframe));
                    render_graph.set_node_input(soft_node, "in_value".to_string(), Boolean(soft_shading));
                    render_graph.set_node_input(tess_node, "in_value".to_string(), Float(tesselation_level));

                    render_graph.update(&gl, 0.0);
                    render_graph.evaluate(&gl);

                    gui_renderer.render(ui);

                    gl.finish();
                    w_ctx.swap_buffers().unwrap();
                },
                _ => (),
            }
        });
    }
}
