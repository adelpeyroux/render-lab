#version 410 core

layout (vertices = 3) out;

uniform float tess_level;

in VS_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} tesc_in[];

out TESC_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} tesc_out[];

void main ()
{
    tesc_out[gl_InvocationID].pos = tesc_in[gl_InvocationID].pos;
    tesc_out[gl_InvocationID].normal = tesc_in[gl_InvocationID].normal;
    tesc_out[gl_InvocationID].uv = tesc_in[gl_InvocationID].uv;

    gl_TessLevelOuter[0] = tess_level;
    gl_TessLevelOuter[1] = tess_level;
    gl_TessLevelOuter[2] = tess_level;
    gl_TessLevelInner[0] = tess_level;
}